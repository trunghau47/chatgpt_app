// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:chatgpt_app/models/chat_model.dart';
import 'package:chatgpt_app/save_chat/chat_save.dart';

abstract class ChatState {}

class ChatInitial extends ChatState {}

class LoadChatSate extends ChatState {
  List<SaveChat> saveChat;
  LoadChatSate({
    required this.saveChat,
  });
}

class ChatContentState extends ChatState {
  // ChatModel response;
  // ChatContentState({
  //   required this.response,
  // });
  List<SaveChat> saveChat;
  ChatContentState({
    required this.saveChat,
  });
}

class ChatSaveState extends ChatState {
  List<SaveChat> saveChat;
  ChatSaveState({
    required this.saveChat,
  });
}

class ChatDeleteTokenState extends ChatState {
  List<SaveChat> deleteChat;
  ChatDeleteTokenState({
    required this.deleteChat,
  });
}

class PullToRefreshChatState extends ChatState {
  List<SaveChat> listChat;
  PullToRefreshChatState({
    required this.listChat,
  });
}
