// ignore_for_file: no_leading_underscores_for_local_identifiers
import 'dart:convert';
import 'package:chatgpt_app/Chat/bloc/chat_event.dart';
import 'package:chatgpt_app/models/chat_model.dart';
import 'package:chatgpt_app/Chat/bloc/chat_state.dart';
import 'package:chatgpt_app/save_chat/chat_save.dart';
import 'package:chatgpt_app/config/stogare.dart';
import 'package:chatgpt_app/key/key.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

const endpoint = "https://api.openai.com/v1/chat/completions";

var header = {
  ///! $OPENAI_API_KEY Là key được lấy từ OpenAI cá nhân
  "Authorization": "Bearer $OPENAI_API_KEY",
  "Content-Type": "application/json",
};
var client = http.Client();

class API {
  late int statusCode;
  Future<ChatModel> question(String content) async {
    var paramContent = <String, dynamic>{};
    paramContent["role"] = "user";
    paramContent["content"] = content;

    var paramData = <String, dynamic>{};
    paramData["model"] = "gpt-3.5-turbo";
    paramData["messages"] = [paramContent];
    var response = await client.post(Uri.parse(endpoint),
        headers: header, body: jsonEncode(paramData));
    // ignore: avoid_print
    print("Check Status code || ${response.statusCode}");
    statusCode = response.statusCode;
    return ChatModel.fromJson(json.decode(utf8.decode(response.bodyBytes)));
  }
}

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc() : super(ChatInitial()) {
    on<ChatEvent>(
      (event, emit) async {
        if (event is LoadChatEvent) {
          List chatList = await Storage().readJsonList();
          List<SaveChat> listSave = chatList
              .map((e) => SaveChat(
                    id: e['id'],
                    role: e['role'],
                    content: e['content'],
                  ))
              .toList();
          emit(LoadChatSate(saveChat: listSave));
        } else if (event is ChatContentEvent) {
          try {
            List chatList = await Storage().readJsonList();
            if (chatList.isNotEmpty) {
              List<SaveChat> listSave = chatList
                  .map((e) => SaveChat(
                        id: e['id'],
                        role: e['role'],
                        content: e['content'],
                      ))
                  .toList();
              var maxID = findMaxId(listSave);
              Map<String, dynamic> chat = {
                'id': maxID + 1,
                'role': "user",
                'content': event.content,
              };
              chatList.add(chat);
              await Storage().saveJsonList(chatList);
              List<SaveChat> listExport = chatList
                  .map((e) => SaveChat(
                        id: e['id'],
                        role: e['role'],
                        content: e['content'],
                      ))
                  .toList();
              emit(ChatContentState(saveChat: listExport));
            } else {
              Map<String, dynamic> chat = {
                'id': 1,
                'role': "user",
                'content': event.content,
              };
              chatList.add(chat);
              await Storage().saveJsonList(chatList);
              List<SaveChat> listExport = chatList
                  .map((e) => SaveChat(
                        id: e['id'],
                        role: e['role'],
                        content: e['content'],
                      ))
                  .toList();
              emit(ChatContentState(saveChat: listExport));
            }
            // }
          } on Exception catch (e) {
            throw Exception(e);
          }
        } else if (event is ChatSaveEvent) {
          try {
            List chatList = await Storage().readJsonList();
            if (chatList.isNotEmpty) {
              List<SaveChat> listSave = chatList
                  .map((e) => SaveChat(
                        id: e['id'],
                        role: e['role'],
                        content: e['content'],
                      ))
                  .toList();

              var maxID = findMaxId(listSave);
              final API _api = API();
              var response = await _api.question(event.content);
              var mess = response.choices![0].message;
              Map<String, dynamic> chatBot = {
                'id': maxID + 1,
                'role': mess!.role,
                'content': mess.content,
              };
              // chatList.add(chat);
              chatList.add(chatBot);
              await Storage().saveJsonList(chatList);
              List<SaveChat> listExport = chatList
                  .map((e) => SaveChat(
                        id: e['id'],
                        role: e['role'],
                        content: e['content'],
                      ))
                  .toList();
              emit(ChatSaveState(saveChat: listExport));
            }
          } on Exception catch (e) {
            throw Exception(e);
          }
        } else if (event is ChatDeletoTokenEvent) {
          try {
            await Storage().deleteList();
            var list = await Storage().readJsonList();
            List<SaveChat> listExport = list
                .map((e) => SaveChat(
                      id: e['id'],
                      role: e['role'],
                      content: e['content'],
                    ))
                .toList();
            emit(ChatDeleteTokenState(deleteChat: listExport));
          } on Exception catch (e) {
            throw Exception(e);
          }
        } else if (event is PullToRefreshChatEvent) {
          List chatList = await Storage().readJsonList();
          List<SaveChat> listSave = chatList
              .map((e) => SaveChat(
                    id: e['id'],
                    role: e['role'],
                    content: e['content'],
                  ))
              .toList();
          emit(PullToRefreshChatState(listChat: listSave));
        }
      },
    );
  }
}

/// Hàm tìm ID lớn nhất trong list tin nhắn đã lưu
///
int findMaxId(List<SaveChat> jsonList) {
  int maxId = 0;
  for (SaveChat item in jsonList) {
    int id = item.id;

    if (id > maxId) {
      maxId = id;
    }
  }
  return maxId;
}
