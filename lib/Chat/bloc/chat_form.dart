// ignore_for_file: no_leading_underscores_for_local_identifiers
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:chatgpt_app/Chat/bloc/bloc.dart';
import 'package:chatgpt_app/save_chat/chat_save.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:flutter_highlight/themes/agate.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ChatForm extends StatefulWidget {
  const ChatForm({super.key});

  @override
  State<ChatForm> createState() => _ChatFormState();
}

class _ChatFormState extends State<ChatForm> {
  final TextEditingController _contentController = TextEditingController();
  List<SaveChat>? _saveChat = [];
  final _focusNode = FocusNode();
  final ScrollController _scrollController = ScrollController();
  int line = 1;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ChatBloc>(context).add(LoadChatEvent());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            "Create toppic message",
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Inter',
                fontSize: 18,
                fontWeight: FontWeight.w600),
          ),
        ),
        backgroundColor: Colors.white,
        leading: Builder(
          builder: (context) {
            return IconButton(
              icon: SvgPicture.asset(
                "assets/svg/figma-arrow-back.svg",
                height: 25,
                width: 25,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.more_vert,
              color: Colors.black,
            ),
            onPressed: () async {
              dialogComfirmWidget("jsonList", context).then(
                (value) {
                  if (value != null) {
                    BlocProvider.of<ChatBloc>(context)
                        .add(ChatDeletoTokenEvent(nameToken: value));
                    Fluttertoast.showToast(
                      msg: "Xóa Thành Công!!!",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Colors.blue[600],
                      textColor: Colors.white,
                      timeInSecForIosWeb: 5,
                      fontSize: 14,
                    );
                  }
                },
              );
            },
          ),
        ],
      ),
      body: BlocListener<ChatBloc, ChatState>(
        listener: (context, state) async {
          if (state is LoadChatSate) {
            _saveChat = state.saveChat;
          } else if (state is ChatContentState) {
            // var res = await state.response.choices![0];
            // _messageBot = res.message!.content!;
            _saveChat = state.saveChat;
          } else if (state is ChatSaveState) {
            _saveChat = state.saveChat;
          } else if (state is ChatDeleteTokenState) {
            // dialogComfirmWidget(, context);
            _saveChat = state.deleteChat;
          }
        },
        child: BlocBuilder<ChatBloc, ChatState>(
          builder: (context, state) {
            return _bodyWiget();
          },
        ),
      ),
    );
  }

  Widget buildListTile(
      String title, IconData icon, void Function()? tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: const TextStyle(
          fontFamily: 'Inter',
          fontSize: 20,
          color: Colors.black,
        ),
      ),
      onTap: tapHandler,
    );
  }

  Future<dynamic> dialogComfirmWidget(String listDelete, BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              color: Colors.blue[600],
              child: const Center(
                child: Text(
                  "Thông Báo",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            const Padding(
              padding: EdgeInsets.all(10.0),
              child: SizedBox(
                child: Text(
                  "Nếu Xác Nhận Xóa Bạn Không Thể Khôi Phục Lại Đoạn Chat!!",
                  style: TextStyle(
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            // const SizedBox(height: 30),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 140,
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {
                      // String text = "jsonList";
                      Navigator.of(context).pop(listDelete);
                    },
                    child: const Text("Xóa"),
                  ),
                ),
                SizedBox(
                  width: 140,
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("Hủy"),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  Widget _bodyWiget() {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<ChatBloc>(context).add(PullToRefreshChatEvent());
      },
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              controller: _scrollController,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: _listViewWidget(),
              ),
            ),
          ),
          _boxInputText(),
        ],
      ),
    );
  }

  ListView _listViewWidget() {
    return ListView.builder(
      itemCount: _saveChat!.length,
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        final _list = _saveChat![index];
        return _chatContainer(_list.content, _list.role);
      },
    );
  }

  Wrap _chatContainer(String text, String role) {
    RegExp exp = RegExp(r'```([\s\S]*?)```|([^\`]+)');
    List<Widget> textWidgets = [];
    Iterable<Match> matches = exp.allMatches(text);
    for (Match match in matches) {
      if (match.group(1) != null) {
        String? code = match.group(1);
        textWidgets.add(
          Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                  color: Colors.grey[600],
                ),
                height: 30,
                width: MediaQuery.of(context).size.width - 50,
                child: GestureDetector(
                  onTap: () {
                    Clipboard.setData(
                      ClipboardData(text: code),
                    );
                    Fluttertoast.showToast(
                      msg: "Đã Sao Chép",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Colors.blue[600],
                      textColor: Colors.white,
                      timeInSecForIosWeb: 3,
                      fontSize: 14,
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Icon(
                        Icons.copy,
                        color: Colors.white,
                        size: 15,
                      ),
                      SizedBox(width: 5),
                      Text(
                        "Copy",
                        style: TextStyle(
                          fontFamily: "Inter",
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(width: 10),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width - 50,
                child: HighlightView(
                  padding: const EdgeInsets.only(left: 10),
                  code!,
                  textStyle: const TextStyle(
                    fontSize: 17,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w400,
                    // color: Colors.white,
                  ),
                  language: "Dart",
                  theme: agateTheme,
                  tabSize: 8,
                ),
              ),
            ],
          ),
        );
      } else {
        String? text = match.group(2);
        textWidgets.add(
          SelectableText(
            text!,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 17,
              fontFamily: 'Inter',
              fontWeight: FontWeight.w400,
            ),
          ),
        );
      }
    }
    return Wrap(
      alignment:
          role.compareTo("user") == 0 ? WrapAlignment.end : WrapAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 0, top: 4, bottom: 4, right: 0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24),
              border: Border.all(width: 1, color: Colors.grey),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Wrap(children: textWidgets),
            ),
          ),
        ),
        role.compareTo("user") == 0
            ? Padding(
                padding: const EdgeInsets.only(
                    left: 38 - 16, right: 34 - 16, bottom: 30, top: 4),
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          const Text(
                            "Just Now",
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Color(0x6C727580),
                            ),
                          ),
                          const SizedBox(width: 14),
                          Container(
                            height: 24,
                            width: 38,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(8),
                              ),
                              color: Color(0xffe8ecef),
                            ),
                            child: const Center(
                              child: Text("Edit"),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 32,
                      width: 32,
                      child: CircleAvatar(
                        foregroundImage: AssetImage("assets/img/CF-2.jpg"),
                      ),
                    ),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.only(
                  left: 22,
                  right: 34 - 16,
                  bottom: 30,
                  top: 4,
                ),
                child: Row(
                  children: [
                    const SizedBox(
                      height: 32,
                      width: 32,
                      child: CircleAvatar(
                        foregroundImage:
                            AssetImage("assets/img/chatgpt_logo.png"),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            onPressed: () {
                              Clipboard.setData(
                                ClipboardData(text: text),
                              );
                              Fluttertoast.showToast(
                                msg: "Đã Sao Chép",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                backgroundColor: Colors.blue[600],
                                textColor: Colors.white,
                                timeInSecForIosWeb: 3,
                                fontSize: 14,
                              );
                            },
                            icon: const Icon(
                              Icons.copy,
                              color: Colors.grey,
                              size: 20,
                            ),
                          ),
                          const SizedBox(width: 5),
                          const Text(
                            "Just Now",
                            style: TextStyle(
                              fontFamily: 'Inter',
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              color: Color(0x6C727580),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
        const SizedBox(height: 30),
      ],
    );
  }

  Padding _boxInputText() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: TextFormField(
          focusNode: _focusNode,
          controller: _contentController,
          maxLines: null,
          textInputAction: TextInputAction.newline,
          decoration: InputDecoration(
            hintText: "Message / Tag",
            isDense: true,
            alignLabelWithHint: true,
            contentPadding: const EdgeInsets.symmetric(vertical: 30.0),
            prefixIcon: IconButton(
                onPressed: () {}, icon: const Icon(Icons.add_circle)),
            suffixIcon: IconButton(
              icon: SvgPicture.asset("assets/svg/figma-send.svg"),
              onPressed: () {
                BlocProvider.of<ChatBloc>(context)
                    .add(ChatContentEvent(content: _contentController.text));
                BlocProvider.of<ChatBloc>(context).add(ChatSaveEvent(
                    id: 1, role: "ai", content: _contentController.text));

                _contentController.clear();

                Fluttertoast.showToast(
                  msg: "Chờ Một Xíu!!!",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  backgroundColor: Colors.blue[600],
                  textColor: Colors.white,
                  timeInSecForIosWeb: 5,
                  fontSize: 14,
                );
              },
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        ),
      ),
    );
  }
}

//  Tạo




///code lưu
///
// Padding(
//   padding: const EdgeInsets.only(left: 18, top: 10, right: 38),
//   child: Row(
//     children: [
//       const CircleAvatar(
//         foregroundImage: AssetImage("assets/img/CF-2.jpg"),
//       ),
//       const SizedBox(width: 8),
//       Expanded(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             const Text(
//               "Just Now",
//               style: TextStyle(
//                 fontFamily: 'Inter',
//                 fontWeight: FontWeight.w400,
//                 fontSize: 12,
//                 color: Color(0x6C727580),
//               ),
//             ),
//             const SizedBox(height: 4),
//             Container(
//               width: double.infinity,
//               decoration: const BoxDecoration(
//                 borderRadius: BorderRadius.all(
//                   Radius.circular(8),
//                 ),
//                 color: Color(0xffe8ecef),
//               ),
//               child: const Padding(
//                 padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
//                 child: Text(
//                   "Stop Generate",
//                   textAlign: TextAlign.center,
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ],
//   ),
// ),
