// ignore_for_file: public_member_api_docs, sort_constructors_first

abstract class ChatEvent {}

class PullToRefreshChatEvent extends ChatEvent {}

class LoadChatEvent extends ChatEvent {}

class ChatContentEvent extends ChatEvent {
  String content;
  ChatContentEvent({
    required this.content,
  });
}

class ChatSaveEvent extends ChatEvent {
  int id;
  String role;
  String content;
  ChatSaveEvent({
    required this.id,
    required this.role,
    required this.content,
  });
}

class ChatDeletoTokenEvent extends ChatEvent {
  String nameToken;
  ChatDeletoTokenEvent({
    required this.nameToken,
  });
}
