import 'package:chatgpt_app/Chat/bloc/bloc.dart';
import 'package:chatgpt_app/Chat/bloc/chat_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatScreen extends StatelessWidget {
  const ChatScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ChatBloc>(
          create: (context) => ChatBloc(),
        ),
      ],
      child: const ChatForm(),
    );
  }
}
