// ignore_for_file: library_private_types_in_public_api
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomTabBar extends StatefulWidget {
  final List<String> tabs;
  final Function(int) onTap;

  const CustomTabBar({
    Key? key,
    required this.tabs,
    required this.onTap,
  }) : super(key: key);

  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    // final theme = Theme.of(context);
    final width = MediaQuery.of(context).size.width;
    final tabWidth = width / widget.tabs.length - 15;
    return SizedBox(
      height: 50,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                for (var i = 0; i < widget.tabs.length; i++)
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = i;
                      });
                      widget.onTap(i);
                    },
                    child: Container(
                      width: tabWidth,
                      height: double.infinity,
                      decoration: BoxDecoration(
                        color:
                            _selectedIndex == i ? Colors.black : Colors.white,
                        border: Border.all(color: Colors.black, width: 2),
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            widget.tabs[i].compareTo("All Types") == 0
                                ? _selectedIndex == i
                                    ? SvgPicture.asset(
                                        "assets/svg/figma-huge-white.svg")
                                    : SvgPicture.asset(
                                        "assets/svg/figma-huge-black.svg")
                                : widget.tabs[i].compareTo("Chat") == 0
                                    ? _selectedIndex == i
                                        ? SvgPicture.asset(
                                            "assets/svg/figma-picture-white.svg")
                                        : SvgPicture.asset(
                                            "assets/svg/figma-picture-black.svg")
                                    : _selectedIndex == i
                                        ? SvgPicture.asset(
                                            "assets/svg/figma-play-white.svg")
                                        : SvgPicture.asset(
                                            "assets/svg/figma-play-black.svg"),
                            Text(
                              widget.tabs[i],
                              style: TextStyle(
                                color: _selectedIndex == i
                                    ? Colors.white
                                    : Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                fontFamily: 'Inter',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 200),
            curve: Curves.easeInOut,
            left: _selectedIndex * tabWidth,
            bottom: 0,
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 200),
              curve: Curves.easeInOut,
              height: 2,
              width: tabWidth,
            ),
          ),
        ],
      ),
    );
  }
}
