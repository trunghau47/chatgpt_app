import 'package:chatgpt_app/Chat/chat_screen.dart';
import 'package:chatgpt_app/Generate_Image/generate_image_screen.dart';
import 'package:flutter/material.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.white,
      child: Column(
        children: [
          Expanded(
              child: ListView(
            children: [
              const UserAccountsDrawerHeader(
                accountName: Text('Xin Chào', style: TextStyle(fontSize: 20)),
                accountEmail:
                    Text("Nguyễn Trung Hậu", style: TextStyle(fontSize: 18)),
                currentAccountPicture: CircleAvatar(
                    foregroundImage: AssetImage("assets/img/CF-2.jpg")),
              ),
              buildListTile('Chat', Icons.chat, () {
                Navigator.pop(context);
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (BuildContext context) => const ChatScreen()),
                );
              }),
              buildListTile('Generate Image', Icons.image, () {
                Navigator.pop(context);
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const GenerateImageScreen()),
                );
              }),
            ],
          ))
        ],
      ),
    );
  }

  Widget buildListTile(
      String title, IconData icon, void Function()? tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: const TextStyle(
            // fontFamily: 'RobotoCondensed',
            fontSize: 20,
            color: Colors.black),
      ),
      onTap: tapHandler,
    );
  }
}
