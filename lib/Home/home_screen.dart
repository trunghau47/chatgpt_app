// ignore_for_file: sort_child_properties_last

import 'package:chatgpt_app/Chat/chat_screen.dart';
import 'package:chatgpt_app/Generate_Image/generate_image_screen.dart';
import 'package:chatgpt_app/Home/components/drawer.dart';
import 'package:chatgpt_app/Home/components/custom_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _bodyWidget(),
      drawer: const DrawerWidget(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      leading: Padding(
        padding: const EdgeInsets.only(left: 16),
        child: Builder(
          builder: (context) {
            return SizedBox(
              height: 48,
              width: 48,
              child: IconButton(
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
                icon: SvgPicture.asset(
                  "assets/svg/icons8-equal.svg",
                  height: 20,
                  width: 32,
                ),
              ),
            );
          },
        ),
      ),
      actions: const [
        Padding(
          padding: EdgeInsets.only(right: 16),
          child:
              CircleAvatar(foregroundImage: AssetImage("assets/img/CF-2.jpg")),
        ),
      ],
    );
  }

  Stack _bodyWidget() {
    return Stack(
      children: [
        Column(
          children: [
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: "Tìm Kiếm Danh Sách Chat",
                  hintStyle: TextStyle(
                      fontFamily: 'Inter',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0),
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 36),
            Row(
              children: const [
                SizedBox(width: 16),
                Text(
                  "Recent",
                  style: TextStyle(
                    fontFamily: 'Inter',
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.start,
                )
              ],
            ),
            const SizedBox(height: 20),
            _recentWidget(),
            Padding(
              padding: const EdgeInsets.only(top: 35, left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    "Chat List",
                    style: TextStyle(
                        fontFamily: 'Inter',
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    "See all",
                    style: TextStyle(
                      fontFamily: 'Inter',
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Color(0xFF0084FF),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            CustomTabBar(
              tabs: const ['All Types', 'Chat', 'Image'],
              onTap: (index) {},
            ),
            _chatListWidget(),
          ],
        ),
        Positioned(
          bottom: 20,
          right: 20,
          child: PopupMenuButton(
            offset: const Offset(-50, -100),
            itemBuilder: (BuildContext context) {
              return <PopupMenuEntry>[
                PopupMenuItem(
                  child: Row(
                    children: const [
                      Icon(Icons.chat),
                      SizedBox(width: 10),
                      Text("Chat")
                    ],
                  ),
                  value: 1,
                ),
                PopupMenuItem(
                  child: Row(
                    children: const [
                      Icon(Icons.image),
                      SizedBox(width: 10),
                      Text("Generate Image")
                    ],
                  ),
                  value: 2,
                ),
              ];
            },
            onSelected: (selectedValue) {
              switch (selectedValue) {
                case 1:
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => const ChatScreen(),
                    ),
                  );
                  break;
                case 2:
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const GenerateImageScreen(),
                    ),
                  );
                  break;
                default:
                  Navigator.pop(context);
              }
            },
            child: Container(
              height: 56,
              width: 56,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(90),
                color: Colors.black,
              ),
              child: const Center(
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  SingleChildScrollView _recentWidget() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          const SizedBox(width: 20),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        const GenerateImageScreen()),
              );
            },
            child: Container(
              height: 163.5,
              width: 163.5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                gradient: const LinearGradient(
                  colors: [
                    Color.fromARGB(255, 133, 175, 234),
                    Color.fromARGB(255, 11, 56, 203)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    child: Text(
                      "Generate img loading and upload ",
                      style: TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFFFFFFFF)),
                    ),
                  ),
                  Expanded(
                      child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.5),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16),
                        ),
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          "assets/svg/figma-picture-white.svg",
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          ),
          const SizedBox(width: 8.5),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) => const ChatScreen()),
              );
            },
            child: Container(
              height: 163.5,
              width: 163.5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                gradient: const LinearGradient(
                  colors: [
                    Color.fromARGB(255, 210, 110, 202),
                    Color.fromARGB(255, 93, 38, 211)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    child: Text(
                      "Write code (HTML, CSS and JS) for a simple welcome page and form with 3 input fields and a dropdown with 2 buttons, cancel and send, then run test with my Codepen project. ",
                      style: TextStyle(
                        fontFamily: 'Inter',
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFFFFFFFF),
                      ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Expanded(
                      child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.5),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16),
                        ),
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          "assets/svg/figma-code.svg",
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          ),
          const SizedBox(width: 8.5),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) => const ChatScreen()),
              );
            },
            child: Container(
              height: 163.5,
              width: 163.5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                gradient: const LinearGradient(
                  colors: [
                    Color.fromARGB(255, 210, 110, 202),
                    Color.fromARGB(255, 93, 38, 211)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    child: Text(
                      "Write code (HTML, CSS and JS) for a simple welcome page and form with 3 input fields and a dropdown with 2 buttons, cancel and send, then run test with my Codepen project. ",
                      style: TextStyle(
                        fontFamily: 'Inter',
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFFFFFFFF),
                      ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Expanded(
                      child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.5),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16),
                        ),
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          "assets/svg/figma-code.svg",
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          ),
          const SizedBox(width: 8.5),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        const GenerateImageScreen()),
              );
            },
            child: Container(
              height: 163.5,
              width: 163.5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                gradient: const LinearGradient(
                  colors: [
                    Color.fromARGB(255, 133, 175, 234),
                    Color.fromARGB(255, 11, 56, 203)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    child: Text(
                      "Generate img loading and upload ",
                      style: TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFFFFFFFF)),
                    ),
                  ),
                  Expanded(
                      child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.5),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16),
                        ),
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          "assets/svg/figma-picture-white.svg",
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          ),
          const SizedBox(width: 8.5),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        const GenerateImageScreen()),
              );
            },
            child: Container(
              height: 163.5,
              width: 163.5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                gradient: const LinearGradient(
                  colors: [
                    Color.fromARGB(255, 133, 175, 234),
                    Color.fromARGB(255, 11, 56, 203)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    child: Text(
                      "Generate img loading and upload ",
                      style: TextStyle(
                          fontFamily: 'Inter',
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFFFFFFFF)),
                    ),
                  ),
                  Expanded(
                      child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.5),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(16),
                          bottomRight: Radius.circular(16),
                        ),
                      ),
                      child: Center(
                        child: SvgPicture.asset(
                          "assets/svg/figma-picture-white.svg",
                        ),
                      ),
                    ),
                  ))
                ],
              ),
            ),
          ),
          const SizedBox(width: 8.5),
        ],
      ),
    );
  }

  //chatlist
  Expanded _chatListWidget() {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.only(left: 24, right: 24, top: 16),
          child: Column(
            children: [
              Row(
                children: [
                  SizedBox(
                    height: 88,
                    width: 88,
                    child: Image.asset("assets/img/figma-education.png"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "EDUCATION",
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/img/Avatar.png",
                              height: 24,
                              width: 60,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text("1m"),
                          ],
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SvgPicture.asset("assets/svg/figma-share.svg"),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              //
              const SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(
                    height: 88,
                    width: 88,
                    child: Image.asset("assets/img/figma-content.png"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "CONTENT",
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/img/Avatar.png",
                              height: 24,
                              width: 60,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text("1m"),
                          ],
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SvgPicture.asset("assets/svg/figma-share.svg"),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              //
              const SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(
                    height: 88,
                    width: 88,
                    child: Image.asset("assets/img/figma-question.png"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "QUESTION",
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/img/Avatar.png",
                              height: 24,
                              width: 60,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text("1m"),
                          ],
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SvgPicture.asset("assets/svg/figma-share.svg"),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              //
              const SizedBox(height: 24),
              Row(
                children: [
                  SizedBox(
                    height: 88,
                    width: 88,
                    child: Image.asset("assets/img/figma-education.png"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const Text(
                          "EDUCATION",
                          style: TextStyle(
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/img/Avatar.png",
                              height: 24,
                              width: 60,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            Text("1m"),
                          ],
                        ),
                        const SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SvgPicture.asset("assets/svg/figma-share.svg"),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
