// ignore_for_file: public_member_api_docs, sort_constructors_first, depend_on_referenced_packages, import_of_legacy_library_into_null_safe
import 'dart:io';
import 'dart:math';
import 'package:chatgpt_app/Preview_Image/download_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
// import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:photo_view/photo_view.dart';
import 'package:uuid/uuid.dart';

class PreviewImageScreen extends StatefulWidget {
  final String url;

  const PreviewImageScreen({
    Key? key,
    required this.url,
  }) : super(key: key);

  @override
  State<PreviewImageScreen> createState() => _PreviewImageScreenState();
}

class _PreviewImageScreenState extends State<PreviewImageScreen> {
  // void _onImageSaveButtonPressed(String url, Response response) async {
  //   // print("_onImageSaveButtonPressed");
  //   // var response = await http.get(Uri.parse(url));
  //   // debugPrint(response.statusCode.toString());

  //   var uuid = const Uuid().v1();
  //   var filePath = await ImagePickerSaver.saveFile(
  //       fileData: response.bodyBytes, title: "Trunghau47-$uuid");

  //   var savedFile = File.fromUri(Uri.file(filePath));

  //   if (await savedFile.exists()) {
  //     // kiểm tra file đã được lưu trữ chưa
  //     setState(() {
  //       var _imageFile = Future<File>.sync(() => savedFile);
  //     });
  //   } else {}
  // }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () async {
                Fluttertoast.showToast(
                  msg: "Đang Tải Xuống Hình Ảnh",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  backgroundColor: Colors.blue[600],
                  textColor: Colors.white,
                  timeInSecForIosWeb: 5,
                  fontSize: 14,
                );

                var response = await Dio().get(widget.url,
                    options: Options(responseType: ResponseType.bytes));
                print("Check DIO 71|| ${response.statusMessage}");
                if (response.statusCode == 200) {
                  DownLoadImage().downloadImage(widget.url);
                  Fluttertoast.showToast(
                      msg: "Đã Lưu Hình Ảnhx",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Colors.blue[600],
                      textColor: Colors.white,
                      timeInSecForIosWeb: 3,
                      fontSize: 14);
                  // _onImageSaveButtonPressed(widget.url, response);
                  _dialogComfirmWidget("Đã Tải Xong");
                } else if (response.statusCode == 403) {
                  _dialogComfirmWidget(
                      "Hình Ảnh Đã Hết Hạn Hoặc Lỗi Vui Lòng Tạo Một Hình Khác");
                }
              },
              icon: const Icon(Icons.download))
        ],
      ),
      body: SizedBox(
        height: height,
        width: width,
        child: PhotoView(imageProvider: NetworkImage(widget.url)),
      ),
    );
  }

  Future<dynamic> _dialogComfirmWidget(String textNoti) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              color: Colors.blue[600],
              child: const Center(
                child: Text(
                  "Thông Báo",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                child: Text(
                  textNoti,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            // const SizedBox(height: 30),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 140,
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("Xác Nhận"),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}








// Theme.of(context).platform == Platform.isAndroid
//                           ? const Icon(Icons.arrow_back)
//                           : const Icon(Icons.arrow_back_ios_new)),