class ImageModel {
  ImageModel({
    required this.created,
    required this.data,
  });
  late final int created;
  late final List<Data> data;

  ImageModel.fromJson(Map<String, dynamic> json) {
    created = json['created'];
    data = List.from(json['data']).map((e) => Data.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['created'] = created;
    _data['data'] = data.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Data {
  Data({
    required this.url,
  });
  late final String url;

  Data.fromJson(Map<String, dynamic> json) {
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['url'] = url;
    return _data;
  }
}
