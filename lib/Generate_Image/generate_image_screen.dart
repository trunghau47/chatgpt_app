import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:chatgpt_app/Generate_Image/generate_image_bloc/bloc_generate_image.dart';
import 'package:chatgpt_app/Generate_Image/generate_image_bloc/generate_image_form.dart';

class GenerateImageScreen extends StatelessWidget {
  const GenerateImageScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<GenerateImageBloc>(
            create: (context) => GenerateImageBloc())
      ],
      child: GenerateImageForm(
        key: key,
      ),
    );
  }
}
