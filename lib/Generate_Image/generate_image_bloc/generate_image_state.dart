// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:chatgpt_app/models/image_model.dart';
import 'package:chatgpt_app/save_chat/image_save.dart';

abstract class GenerateImageState {}

class InitialGenerateImage extends GenerateImageState {}

class ImageLoadDataSate extends GenerateImageState {
  List<SaveImage> listImage;
  ImageLoadDataSate({
    required this.listImage,
  });
}

class ImageSaveState extends GenerateImageState {
  List<SaveImage> listImage;
  ImageSaveState({
    required this.listImage,
  });
}

class ChatImageState extends GenerateImageState {
  List<SaveImage> listImage;
  ChatImageState({
    required this.listImage,
  });
}

class ImageDeleteTokenState extends GenerateImageState {
  List<SaveImage> deleteChat;
  ImageDeleteTokenState({
    required this.deleteChat,
  });
}
