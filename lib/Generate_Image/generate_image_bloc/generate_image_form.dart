// ignore_for_file: unrelated_type_equality_checks

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dio/dio.dart';
import 'package:chatgpt_app/Preview_Image/download_image.dart';
import 'package:chatgpt_app/Preview_Image/preview_image_screen.dart';
import 'package:chatgpt_app/save_chat/image_save.dart';
import 'package:chatgpt_app/Generate_Image/generate_image_bloc/bloc_generate_image.dart';

class GenerateImageForm extends StatefulWidget {
  const GenerateImageForm({super.key});

  @override
  State<GenerateImageForm> createState() => _GenerateImageFormState();
}

class _GenerateImageFormState extends State<GenerateImageForm> {
  final TextEditingController _contentController = TextEditingController();
  final ScrollController _scrollController = ScrollController();
  List<SaveImage>? _listImage = [];
  final _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<GenerateImageBloc>(context).add(ImageLoadEvent());
    scrollToMaxExtent();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void scrollToMaxExtent() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 100),
        curve: Curves.easeIn,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: BlocListener<GenerateImageBloc, GenerateImageState>(
        listener: (context, state) async {
          if (state is ImageLoadDataSate) {
            _listImage = state.listImage;
          } else if (state is ImageSaveState) {
            _listImage = state.listImage;
          } else if (state is ChatImageState) {
            _listImage = state.listImage;
          } else if (state is ImageDeleteTokenState) {
            _listImage = state.deleteChat;
          }
        },
        child: BlocBuilder<GenerateImageBloc, GenerateImageState>(
          builder: (context, state) {
            return _bodyWidget();
          },
        ),
      ),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Center(
        child: Text(
          "Create toppic message",
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Inter',
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      leading: Builder(
        builder: (context) {
          return IconButton(
            icon: SvgPicture.asset(
              "assets/svg/figma-arrow-back.svg",
              height: 25,
              width: 25,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          );
        },
      ),
      actions: [
        IconButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) => Dialog(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.blue[600],
                      child: const Center(
                        child: Text(
                          "Thông Báo",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),
                    const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: SizedBox(
                        child: Text(
                          "Nếu Xác Nhận Xóa Bạn Không Thể Khôi Phục Lại Đoạn Chat!!",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    // const SizedBox(height: 30),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: 140,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {
                              String text = "image";
                              Navigator.of(context).pop(text);
                            },
                            child: const Text("Xóa"),
                          ),
                        ),
                        SizedBox(
                          width: 140,
                          height: 40,
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text("Hủy"),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ).then(
              (value) {
                if (value != null) {
                  BlocProvider.of<GenerateImageBloc>(context)
                      .add(ImageDeleteTokenEvent(nameToken: value));
                  Fluttertoast.showToast(
                    msg: "Xóa Thành Công!!!",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    backgroundColor: Colors.blue[600],
                    textColor: Colors.white,
                    timeInSecForIosWeb: 5,
                    fontSize: 14,
                  );
                }
              },
            );
          },
          icon: const Icon(
            Icons.more_vert,
            color: Colors.black,
          ),
        )
      ],
    );
  }

  Widget _bodyWidget() {
    return RefreshIndicator(
      onRefresh: () async {
        // BlocProvider.of<GenerateImageBloc>(context).add(());
      },
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              controller: _scrollController,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: _listViewWidget(),
              ),
            ),
          ),
          _boxInputText(),
        ],
      ),
    );
  }

  ListView _listViewWidget() {
    return ListView.builder(
      itemCount: _listImage!.length,
      shrinkWrap: true,
      physics: const BouncingScrollPhysics(),
      itemBuilder: (context, index) {
        final list = _listImage![index];
        return _chatContainer(list.content, list.role);
      },
    );
  }

  GestureDetector _chatContainer(String text, String role) {
    return GestureDetector(
      onTap: () {
        if (role.compareTo("user") != 0) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => PreviewImageScreen(
                url: text,
              ),
            ),
          );
        }
      },
      child: Wrap(
        alignment: role.compareTo("user") == 0
            ? WrapAlignment.end
            : WrapAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 0, top: 4, bottom: 4, right: 0),
            child: GestureDetector(
              onLongPress: () {
                Clipboard.setData(ClipboardData(text: text));
              },
              child: role.compareTo("user") == 0
                  ? Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: SelectableText(
                          text,
                          // softWrap: true,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontFamily: 'Inter',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    )
                  : Stack(
                      children: [
                        // Container(
                        //   height: MediaQuery.of(context).size.height / 3,
                        //   width: MediaQuery.of(context).size.width - 20,
                        //   decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(24),
                        //     image: DecorationImage(
                        //       fit: BoxFit.fill,
                        //       image: CachedNetworkImageProvider(text),
                        //     ),
                        //   ),
                        // ),
                        CachedNetworkImage(
                          imageUrl: text,
                          progressIndicatorBuilder:
                              (context, url, downloadProgress) =>
                                  CircularProgressIndicator(
                                      value: downloadProgress.progress),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                        ),
                        const SizedBox(height: 5),
                        // const Text("Nhấn để xem"),
                        Positioned(
                          bottom: 10,
                          left: 20,
                          right: 20,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Fluttertoast.showToast(
                                    msg: "Test Edit",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    backgroundColor: Colors.blue[600],
                                    textColor: Colors.white,
                                    timeInSecForIosWeb: 5,
                                    fontSize: 14,
                                  );
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: const Color(0xFFFFFFFF),
                                  ),
                                  height: 40,
                                  width: MediaQuery.of(context).size.width / 3,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                            "assets/svg/figma-edit.svg"),
                                        const SizedBox(width: 10),
                                        const Text(
                                          "Edit",
                                          style: TextStyle(
                                            fontFamily: 'Inter',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(width: 5),
                              GestureDetector(
                                onTap: () async {
                                  Fluttertoast.showToast(
                                    msg: "Đang Tải Xuống Hình Ảnh",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    backgroundColor: Colors.blue[600],
                                    textColor: Colors.white,
                                    timeInSecForIosWeb: 5,
                                    fontSize: 14,
                                  );
                                  await DownLoadImage().downloadImage(text);
                                  var response = await Dio().get(text,
                                      options: Options(
                                          responseType: ResponseType.bytes));
                                  if (response.statusCode == 200) {
                                    Fluttertoast.showToast(
                                        msg: "Đã Lưu Hình Ảnhx",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        backgroundColor: Colors.blue[600],
                                        textColor: Colors.white,
                                        timeInSecForIosWeb: 3,
                                        fontSize: 14);
                                    // _onImageSaveButtonPressed(widget.url, response);
                                    _dialogComfirmWidget("Đã Tải Xong");
                                  } else {
                                    _dialogComfirmWidget(
                                        "Hình Ảnh Đã Hết Hạn Hoặc Lỗi Vui Lòng Tạo Một Hình Khác");
                                  }
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: const Color(0xFFFFFFFF),
                                  ),
                                  height: 40,
                                  width: MediaQuery.of(context).size.width / 3,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                            "assets/svg/figma-download.svg"),
                                        const SizedBox(width: 10),
                                        const Text(
                                          "Download",
                                          style: TextStyle(
                                            fontFamily: 'Inter',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
            ),
          ),
          role.compareTo("user") == 0
              ? Padding(
                  // padding: const EdgeInsets.only(
                  //     left: 38, top: 16, right: 34, bottom: 10),
                  padding: const EdgeInsets.only(
                      left: 38 - 16, right: 34 - 16, bottom: 30, top: 4),
                  child: Row(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            const Text(
                              "Just Now",
                              style: TextStyle(
                                fontFamily: 'Inter',
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Color(0x6C727580),
                              ),
                            ),
                            const SizedBox(width: 14),
                            Container(
                              height: 24,
                              width: 38,
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8),
                                ),
                                color: Color(0xffe8ecef),
                              ),
                              child: const Center(
                                child: Text("Edit"),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 32,
                        height: 32,
                        child: CircleAvatar(
                          foregroundImage: AssetImage("assets/img/CF-2.jpg"),
                        ),
                      ),
                    ],
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.only(
                    left: 38 - 16,
                    right: 34 - 16,
                    bottom: 30,
                    top: 4,
                  ),
                  child: Row(
                    children: [
                      const SizedBox(
                        height: 32,
                        width: 32,
                        child: CircleAvatar(
                          foregroundImage:
                              AssetImage("assets/img/chatgpt_logo.png"),
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: const [
                            SizedBox(width: 14),
                            Text(
                              "Just Now",
                              style: TextStyle(
                                fontFamily: 'Inter',
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                color: Color(0x6C727580),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  Padding _boxInputText() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
      child: Align(
        alignment: Alignment.bottomCenter,
        child: TextFormField(
          focusNode: _focusNode,
          controller: _contentController,
          maxLines: null,
          onChanged: (value) {},
          decoration: InputDecoration(
            hintText: "Message / Tag",
            isDense: true,
            alignLabelWithHint: true,
            contentPadding: const EdgeInsets.symmetric(vertical: 30.0),
            prefixIcon: IconButton(
                onPressed: () {}, icon: const Icon(Icons.add_circle)),
            suffixIcon: IconButton(
              icon: SvgPicture.asset("assets/svg/figma-send.svg"),
              onPressed: () {
                BlocProvider.of<GenerateImageBloc>(context).add(ChatSaveEvent(
                    id: 1, role: "user", content: _contentController.text));
                BlocProvider.of<GenerateImageBloc>(context)
                    .add(ImageSaveEvent(content: _contentController.text));

                _contentController.clear();
                Fluttertoast.showToast(
                  msg: "Chờ Một Xíu!!!",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  backgroundColor: Colors.blue[600],
                  textColor: Colors.white,
                  timeInSecForIosWeb: 5,
                  fontSize: 14,
                );
              },
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        ),
      ),
    );
  }

  //
  Future<dynamic> _dialogComfirmWidget(String textNoti) {
    return showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              color: Colors.blue[600],
              child: const Center(
                child: Text(
                  "Thông Báo",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                child: Text(
                  textNoti,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            // const SizedBox(height: 30),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  width: 140,
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("Xác Nhận"),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
