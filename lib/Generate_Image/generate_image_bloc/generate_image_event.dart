// ignore_for_file: public_member_api_docs, sort_constructors_first
abstract class GenerateImageEvent {}

class ImageLoadEvent extends GenerateImageEvent {}

class ImageSaveEvent extends GenerateImageEvent {
  String content;
  ImageSaveEvent({
    required this.content,
  });
}

class ChatSaveEvent extends GenerateImageEvent {
  int id;
  String role;
  String content;
  ChatSaveEvent({
    required this.id,
    required this.role,
    required this.content,
  });
}

class ImageDeleteTokenEvent extends GenerateImageEvent {
  String nameToken;
  ImageDeleteTokenEvent({
    required this.nameToken,
  });
}
