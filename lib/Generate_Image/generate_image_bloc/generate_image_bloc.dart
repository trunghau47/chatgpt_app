import 'dart:convert';
import 'package:chatgpt_app/config/stogare.dart';
import 'package:chatgpt_app/key/key.dart';
import 'package:chatgpt_app/models/image_model.dart';
import 'package:chatgpt_app/save_chat/image_save.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:chatgpt_app/Generate_Image/generate_image_bloc/bloc_generate_image.dart';

const endpoint = "https://api.openai.com/v1/images/generations";

var header = {
  ///! $OPENAI_API_KEY Là key được lấy từ OpenAI cá nhân
  "Authorization": "Bearer $OPENAI_API_KEY",
  "Content-Type": "application/json",
};
var client = http.Client();

class API {
  Future<ImageModel> generations(String content) async {
    var paramContent = <String, dynamic>{};
    paramContent["size"] = "1024x1024";
    paramContent["prompt"] = content;
    paramContent["n"] = 1;

    var response = await client.post(Uri.parse(endpoint),
        headers: header, body: jsonEncode(paramContent));
    // ignore: avoid_print
    print("Check Status code || ${response.statusCode}");
    return ImageModel.fromJson(json.decode(utf8.decode(response.bodyBytes)));
  }
}

class GenerateImageBloc extends Bloc<GenerateImageEvent, GenerateImageState> {
  GenerateImageBloc() : super(InitialGenerateImage()) {
    on<GenerateImageEvent>(
      (event, emit) async {
        if (event is ImageLoadEvent) {
          var listread = await Storage().readListImage();
          List<SaveImage> listImage = listread
              .map((e) => SaveImage(
                    id: e['id'],
                    role: e['role'],
                    content: e['content'],
                  ))
              .toList();
          emit(ImageLoadDataSate(listImage: listImage));
        } else if (event is ChatSaveEvent) {
          //Lưu chat của nguòi dùng
          var listread = await Storage().readListImage();
          if (listread.isNotEmpty) {
            List<SaveImage> listImage = listread
                .map((e) => SaveImage(
                    id: e['id'], role: e['role'], content: e['content']))
                .toList();
            var maxID = findMaxId(listImage);
            Map<String, dynamic> chat = {
              'id': maxID + 1,
              'role': event.role,
              'content': event.content,
            };
            listread.add(chat);

            await Storage().saveListImage(listread);
            List<SaveImage> listExport = listread
                .map((e) => SaveImage(
                    id: e['id'], role: e['role'], content: e['content']))
                .toList();
            emit(ChatImageState(listImage: listExport));
          } else {
            Map<String, dynamic> chat = {
              'id': 1,
              'role': event.role,
              'content': event.content,
            };
            listread.add(chat);
            await Storage().saveListImage(listread);
            List<SaveImage> listExport = listread
                .map((e) => SaveImage(
                    id: e['id'], role: e['role'], content: e['content']))
                .toList();
            emit(ChatImageState(listImage: listExport));
          }
        } else if (event is ImageSaveEvent) {
          //lưu hình ảnh khi api trả về
          try {
            var listread = await Storage().readListImage();
            if (listread.isNotEmpty) {
              List<SaveImage> listImage = listread
                  .map((e) => SaveImage(
                      id: e['id'], role: e['role'], content: e['content']))
                  .toList();
              var maxID = findMaxId(listImage);

              final API api = API();
              var response = await api.generations(event.content);
              var urlImage = response.data[0];
              Map<String, dynamic> image = {
                'id': maxID + 1,
                'role': "ai",
                'content': urlImage.url,
              };
              listread.add(image);
              await Storage().saveListImage(listread);
              List<SaveImage> listExport = listread
                  .map((e) => SaveImage(
                      id: e['id'], role: e['role'], content: e['content']))
                  .toList();
              emit(ImageSaveState(listImage: listExport));
            }
            // else {
            //   // var maxID = findMaxId(listImage);
            //   // Map<String, dynamic> chat = {
            //   //   'id': 1,
            //   //   'role': event.role,
            //   //   'content': event.content,
            //   // };
            //   // listread.add(chat);
            //   //api

            //   final API api = API();
            //   var response = await api.generations(event.content);
            //   var urlImage = response.data[0];
            //   Map<String, dynamic> image = {
            //     'id': 2,
            //     'role': "ai",
            //     'content': urlImage.url,
            //   };
            //   listread.add(image);

            //   await Storage().saveListImage(listread);
            //   List<SaveImage> listExport = listread
            //       .map((e) => SaveImage(
            //           id: e['id'], role: e['role'], content: e['content']))
            //       .toList();
            //   emit(ImageSaveState(listImage: listExport));
            // }
          } on Exception catch (e) {
            throw Exception(e);
          }
        } else if (event is ImageDeleteTokenEvent) {
          try {
            await Storage().deleteListImage();
            var list = await Storage().readListImage();
            List<SaveImage> listExport = list
                .map((e) => SaveImage(
                      id: e['id'],
                      role: e['role'],
                      content: e['content'],
                    ))
                .toList();
            emit(ImageDeleteTokenState(deleteChat: listExport));
          } on Exception catch (e) {
            throw Exception(e);
          }
        }
      },
    );
  }
}

/// Hàm tìm ID lớn nhất trong list tin nhắn đã lưu
///
int findMaxId(List<SaveImage> jsonList) {
  int maxId = 0;
  for (SaveImage item in jsonList) {
    int id = item.id;

    if (id > maxId) {
      maxId = id;
    }
  }
  return maxId;
}
