// ignore_for_file: public_member_api_docs, sort_constructors_first
class SaveChat {
  int id;
  String role;
  String content;
  SaveChat({
    required this.id,
    required this.role,
    required this.content,
  });
}
