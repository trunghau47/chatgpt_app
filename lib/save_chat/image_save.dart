class SaveImage {
  int id;
  String role;
  String content;
  SaveImage({
    required this.id,
    required this.role,
    required this.content,
  });
}
