import 'package:chatgpt_app/Chat/bloc/chat_bloc.dart';

import 'package:chatgpt_app/Home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  debugProfilePaintsEnabled = false;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Chat GPT",
      home: MultiBlocProvider(
        providers: [
          BlocProvider<ChatBloc>(
            create: (context) => ChatBloc(),
          ),
        ],
        // child: const GenerateImageScreen(),
        child: const HomeScreen(),
        // child: const ChatScreen(),
      ),
    );
  }
}
