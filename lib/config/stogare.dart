import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
// Hàm lưu trữ danh sách JSON
  Future<void> saveJsonList(List<dynamic> jsonList) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> stringList = jsonList.map((item) => jsonEncode(item)).toList();
    await prefs.setStringList('jsonList', stringList);
  }

// Hàm đọc danh sách JSON
  Future<List<dynamic>> readJsonList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> stringList = prefs.getStringList('jsonList') ?? [];

    List<dynamic> jsonList =
        stringList.map((item) => jsonDecode(item)).toList();

    return jsonList;
  }

  // xóa danh sách Json
  Future deleteList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('jsonList');
  }

  // Hàm lưu trữ danh sách JSON
  Future<void> saveListImage(List<dynamic> jsonList) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> stringList = jsonList.map((item) => jsonEncode(item)).toList();
    await prefs.setStringList('image', stringList);
  }

// Hàm đọc danh sách JSON
  Future<List<dynamic>> readListImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> stringList = prefs.getStringList('image') ?? [];

    List<dynamic> jsonList =
        stringList.map((item) => jsonDecode(item)).toList();

    return jsonList;
  }

  // xóa danh sách Json
  Future deleteListImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('image');
  }
}
