# Chat GPT App

App để test API của OpenAPI và học cách sử dụng BLOC ^^
- Một số chức năng cơ bản gồm
- [ ] Sử dụng BLOC
- [ ] Sử dụng SharedPeferences để lưu dữ liệu vào local
- [ ] Sử dụng API do OpenAI cung cấp, chưa bắt hết các lỗi khi server trả về
- [ ] Xử lý chưa đồng bộ đoạn chat
## Link Tải Bản Debug

 - Drive APK [Nhấn Để Tải](https://drive.google.com/file/d/1d34AQgAikGzPmx5-EuaUtfMkJUEbS62p/view)
 

## Run Project
- Flutter pub get
- Sử dụng OPENAI_API_KEY từ trang ( https://platform.openai.com/account/api-keys)
- Flutter Run

## Image
- Một số hình ảnh đã khi đã xong app

<img src="https://homestay-giadinhnho.netlify.app/imghau/appchat_gpt_1.png" alt="Home" width="25%">
<img src="https://homestay-giadinhnho.netlify.app/imghau/appchat_gpt_2.png" alt="Drawer" width="25%">
<img src="https://homestay-giadinhnho.netlify.app/imghau/appchat_gpt_3.png" alt="Button New Chat" width="25%">
<img src="https://homestay-giadinhnho.netlify.app/imghau/appchat_gpt_4.png" alt="Chat" width="25%">
<img src="https://homestay-giadinhnho.netlify.app/imghau/appchat_gpt_5.png" alt="Generate Image" width="25%">
<img src="https://homestay-giadinhnho.netlify.app/imghau/appchat_gpt_6.png" alt="Delete Chat" width="25%">



